/*------------------------------------*\
	Document Ready
\*------------------------------------*/
$(document).ready(function(){

	/*------------------------------------*\
		FUNCTION ShowcaseHeightSet()
		Set '.showcase__stage-wrapper'
		height the same as
		'.showcase__stage--active'
		(which is positioned absolutely
		and doesn't fill wrapper)
	\*------------------------------------*/
	function ShowcaseHeightSet() {
		$(".showcase__stage-wrapper").css("height", $(".showcase__stage--active").height() );
	}





	$(window).resize(function(event){
		if( $(".maxwidth1050").css("display") == "none" )
		{
			$(".content").css( "top", $(window).height() );
			$(".hero").css( "height", $(window).height() );
		}

		else
		{
			var HeroHeight = $(".hero__content").height();
			$(".hero").css( "height", HeroHeight );
			$(".content").css( "top", 0 );
		}

		ShowcaseHeightSet();
	});

	window.addEventListener("orientationchange", function() {
		ShowcaseHeightSet();
	});






	/*------------------------------------*\
		Run showcase height function
		after image on active slide
		has loaded
	\*------------------------------------*/
	$(".showcase__stage--active img").one("load", function() {
		ShowcaseHeightSet();
	}).each(function() {
		if(this.complete) $(this).load();
	});






	/*------------------------------------*\
		Hamburger navigation in header
	\*------------------------------------*/
	$(".header__hamburger").click(function(event) {
		$(".header__navigation").fadeToggle(100);
		$(".header__wrapper").toggleClass('is-active');
		event.stopPropagation();
	});

	$(".header__navigation").click(function(event) {
		event.stopPropagation();
	});

	$("html, .header__navigation a").click(function(event) {
		$(".header__navigation").slideUp(100);
		if ($(".header__wrapper").hasClass("is-active")) {
			$(".header__wrapper").toggleClass('is-active');
		}
	});





	/*------------------------------------*\
		Smooth Scroll
	\*------------------------------------*/

	$(".js-smooth-scroll").click(function(event) {
		event.preventDefault();

		var ThisHref = $(this).attr("href");
		//var HeaderHeight = $(".header").height();

		$( ThisHref ).velocity("scroll", {
			duration: 1000,
			//offset: -HeaderHeight,
			easing: "ease",

			begin: function() {
				$(window).on("mousewheel", function(event) {
					return false;
				});
			},

			complete: function() {
				$(window).off("mousewheel");
			}
		});

		window.location.hash = ThisHref;
	});





	/*------------------------------------*\
		Project details 'read more'
		and 'read less' buttons
	\*------------------------------------*/
	$(".showcase__readmore").click(function(event) {
		$(this).parent().parent().find(".showcase__point-details-full").css("display","inline");
		$(this).parent().find(".showcase__ellipsis").css("display","none");
		$(this).hide();

		//set height
		ShowcaseHeightSet();
	});

	$(".showcase__readless").click(function(event) {
		$(this).parent(".showcase__point-details-full").css("display","none");
		$(this).parent().parent().find(".showcase__ellipsis, .showcase__readmore").css("display","inline");

		//set height
		ShowcaseHeightSet();
	});






	/*------------------------------------*\
		Contact form AJAX script
		which communicates with
		contact_form.php
	\*------------------------------------*/
	$("#contact__submit").click(function(event) {
		event.preventDefault();

		var proceed = true;

		if(proceed) // everything looks good! proceed...
		{
			// get input field values data to be sent to server
			post_data = {
				'sender_name'      : String( $('#sender_name').val() ),
				'sender_email'     : String( $('#sender_email').val() ),
				'message_content'  : String( $('#message_content').val() )
			};


			// Ajax post data to server
			$.post('contact_form.php', post_data, function(response)
			{

 				// load json data from server and output message
				// it probably should be refactored a bit...
 				switch (response.type) {

 					case "error_message_content":
						$(".contact__response-ajax-text").hide();
						$(".contact__textarea-wrapper .contact__response-ajax-text").css("display","inline-block").text(response.text);

						$("#message_content, #sender_name, #sender_email").css("box-shadow","none");
						// $("#message_content").css("box-shadow","0 0 10px 0 rgba(255,0,0,0.5)");
						break;

 					case "error_sender_name":
						$(".contact__response-ajax-text").hide();
						$(".contact__input-wrapper--name .contact__response-ajax-text").css("display","inline-block").text(response.text);

						$("#message_content, #sender_name, #sender_email").css("box-shadow","none");
						// $("#sender_name").css("box-shadow","0 0 10px 0 rgba(255,0,0,0.5)");
						break;

 					case "error_sender_email":
						$(".contact__response-ajax-text").hide();
						$(".contact__input-wrapper--email .contact__response-ajax-text").css("display","inline-block").text(response.text);

						$("#message_content, #sender_name, #sender_email").css("box-shadow","none");
						// $("#sender_email").css("box-shadow","0 0 10px 0 rgba(255,0,0,0.5)");
						break;

					default:
						$(".contact__response-ajax-text").hide();

						$("#message_content, #sender_name, #sender_email").css("box-shadow","none");
						$(".contact__button-wrapper").removeClass("ghost-button");
						$(".contact__button-wrapper").addClass("contact__button-wrapper--sent");
						$(".contact__paper-plane-wrapper").addClass("contact__paper-plane-wrapper--takeoff");
						$(".contact__response-description--success").html(response.text);
						$(".contact__response--success").delay(500).fadeIn(100);
						// $(".contact__form input, .contact__form textarea").val("");
						$("#contact__submit").unbind('click');
				}

			}, 'json');

		}
	});





	/*------------------------------------*\
		Steam effect
	\*------------------------------------*/
	//Canvas smoke initialization
	const canvas = document.getElementById('canvas');
	const ctx = canvas.getContext('2d');
	canvas.width = innerWidth;
	canvas.height = innerHeight;

	var backgroundImage = new Image();
	backgroundImage.src = "style/img/hero_background.png";
	const backgroundImageWidth = backgroundImage.width;
	const backgroundImageHeight = backgroundImage.height;

	const party = smokemachine(ctx, [35, 64, 153]);
	party.start(); // start animating
	party.setPreDrawCallback(function(dt){

        // get the original unscaled background image ratio
        var backgroundImageRatio = backgroundImageWidth / backgroundImageHeight;

		// get whole area covered by the background Image. This is the working area of our canvas as well.
		// we want it the same size as the div containing the background image
		let scrollbarWidth = window.innerWidth - document.documentElement.clientWidth; // offset back by the browser scrollbar width
		var coverWidth = innerWidth - scrollbarWidth;
		var coverHeight = innerHeight;
		var coverRatio = coverWidth / coverHeight;

		// figure out the scale factor that would give us the biggest possible image that would fit tightly into current cover area
		// (after scaling the original image width & height by this factor, the image will span the entire cover area either only
		// vertically or only horizontally or both if the cover area's ratio is the same as background image ratio ).
		// The ratio of minRatioSizeX / minRatioSizeY is the same as backgroundImageWidth / backgroundImageHeight
		var scale = Math.min( coverWidth / backgroundImageWidth, coverHeight / backgroundImageHeight );
		var minRatioSizeX = backgroundImageWidth * scale;
		var minRatioSizeY = backgroundImageHeight * scale;

		var scaleToFull = 1.0;
		var offsetX = 0, offsetY = 0;

        // find out which ratio is greater
        if (coverRatio <= backgroundImageRatio ) {
            // If coverRatio <= original backgroundImageRatio, it means that coverHeight is too big compared to coverWidth and
			// esceeds the minRatioSizeY. CoverWidth however is the same as minRatioSizeX.
			// In this case, the minRatio image needs to be scaled up, so that its height is equal to coverHeight
			// and the original backgroundImageRatio is preserved. After scaling the coverWidth however, there will be unnecessary space 
			// horizontally that we need to offset our smoke position.
            scaleToFull = coverHeight / minRatioSizeY;
			let fullWidth = coverWidth * scaleToFull;
			// divided by 2 because our background image is always centered
			offsetX = ( fullWidth - coverWidth ) / 2.0;
        } else {
			// If coverRatio > original backgroundImageRatio, it means that coverWidth is too big compared to coverHeight and
			// esceeds the minRatioSizeX. CoverHeight however is the same as minRatioSizeY.
			// In this case, the minRatio image needs to be scaled up, so that its width is equal to coverWidth
			// and the original backgroundImageRatio is preserved. After scaling the coverHeight however, there will be unnecessary space 
			// vertically that we need to offset our smoke position.
			scaleToFull = coverWidth / minRatioSizeX;
			let fullHeight = coverHeight * scaleToFull;  
			// divided by 2 because our background image is always centered
			offsetY = ( fullHeight - coverHeight ) / 2.0;
		}

		// multiply the scale by the scaleToFull and just use it below
		scale *= scaleToFull;

		var options = {
			minVx: -3/100,
			maxVx: 3/100,
			minVy: -15/100 * scale,
			maxVy: -9/100 * scale,
			minScale: 25 * scale,
			maxScale: 30 * scale,
			minLifetime: 2000,
			maxLifetime: 2000 + 4000 * scale
		}

		// together with calculated offsets and scale we can calculate the final position in any resolution
		const originalResolutionPosX = 892.0;
		const originalResolutionPosY = 625.0;
		let posX = -offsetX + scale * originalResolutionPosX;
		let posY = -offsetY + scale * originalResolutionPosY;

		party.addSmoke(posX, posY, 0.5, options);
		canvas.width = coverWidth;
		canvas.height = coverHeight;

		// debug visualization
		// ctx.fillStyle = "white";
		// ctx.globalAlpha = 0.2;
		// ctx.fillRect(0, 0, minRatioSizeX, minRatioSizeY);
		// ctx.globalAlpha = 1.0;
		// ctx.fillStyle = "red";
		// let sizeX = 20 * scale;
		// let sizeY = 20 * scale;
		// ctx.fillRect(posX - sizeX / 2.0, posY - sizeY/2.0, sizeX, sizeY);
	});

	/*------------------------------------*\
		Set hero and showcase heights - needs to happen after canvas size setup
	\*------------------------------------*/
	$(window).resize();

	/*------------------------------------*\
		Projects carousel
		made with owl carousel
	\*------------------------------------*/
	// owl carousel initializer call
	$('.owl-carousel').owlCarousel({
		items:1,
		loop:true,
		margin:10,
		nav:true,
	});
});
